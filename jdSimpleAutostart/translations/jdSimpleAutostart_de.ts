<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>AddEditDialog</name>
    <message>
        <location filename="../AddEditDialog.py" line="33"/>
        <source>Add new autostart entry</source>
        <translation>Neuen Autostart Eintrag hinzufügen</translation>
    </message>
    <message>
        <location filename="../AddEditDialog.py" line="44"/>
        <source>Edit {{Name}}</source>
        <translation>Bearbeite {{Name}}</translation>
    </message>
    <message>
        <location filename="../AddEditDialog.ui" line="0"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../AddEditDialog.ui" line="0"/>
        <source>Comment:</source>
        <translation>Kommentar:</translation>
    </message>
    <message>
        <location filename="../AddEditDialog.ui" line="0"/>
        <source>Icon:</source>
        <translation>Icon:</translation>
    </message>
    <message>
        <location filename="../AddEditDialog.ui" line="0"/>
        <source>Exec:</source>
        <translation>Befehlszeile:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>In the List below you see all programs in the Autostart of your Desktop</source>
        <translation>In der Liste unten sind alle Autostart Programme deines Desktops zu sehen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>You can enable and disable them with the Checkbox</source>
        <translation>Du kannst sie mit der Checkbox aktivieren und deaktivieren</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Add</source>
        <translation>Hinzufügen</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="0"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
</context>
<context>
    <name>jdSimpleAutostart</name>
    <message>
        <location filename="../__init__.py" line="22"/>
        <source>No autostart directories found</source>
        <translation>Keine Autostart Verzeichnisse gefunden</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="22"/>
        <source>No autostart directories were found. Maybe your Desktop Environment don&apos;t support the autostart specification.</source>
        <translation>Es wurden keine Autostart Verzeichnisse gefunden. Eventuell unterstützt deine Desktopumgebung den Autostart Standard nicht.</translation>
    </message>
</context>
</TS>
