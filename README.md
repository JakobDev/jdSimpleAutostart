# jdSimpleAutostart

# [Moved to Codeberg](https://codeberg.org/JakobDev/jdSimpleAutostart)

jdSimpleAutostart allows you to manage the autostart entries of your Linux Desktop


[Icon source](https://icon-icons.com/icon/rocket-space-startup/96966)
